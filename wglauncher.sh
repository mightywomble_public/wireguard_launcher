#!/bin/bash
## VARIABLES


## FUNCTIONS
func_mainmenu(){
# Bash runction for the main menu, split out into a function as it rill be looped back to
ans=$(zenity  --list  --text "Wireguard HomeVPN" --radiolist  --column "Pick" --column "Item" TRUE "Wireguard UP" FALSE "Wireguard DOWN" FALSE "Wireguard STATUS")
}

func_UP(){
    nmcli connection up wg0
    zenity --question --text "Do you want to check NMCLI status?"
    if [ $? = 0 ]; then
        func_nmcli
    else
        func_mainmenu
    fi
}

func_DOWN(){
    nmcli connection down wg0
    zenity --question --text "Do you want to check NMCLI status?"
    if [ $? = 0 ]; then
        func_nmcli
    else
        func_mainmenu
    fi
}

func_STATUS(){
nmcli --overview connection show wg0 | zenity --text-info --width 600 --height 500
func_mainmenu
}

func_nmcli(){
nmcli | zenity --text-info --width 600 --height 800
func_mainmenu
}


##SCRIPT
func_mainmenu
echo $ans
array=( UP DOWN STATUS )
for i in "${array[@]}"
do
	echo $ans | grep $i
    result=$(echo $?)
    echo $result
    if [ $result = 0 ]; then 
    func_$i
    fi
done